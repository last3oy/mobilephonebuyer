package com.last3oy.mobilephonebuyer.ui.detail

import android.support.v7.widget.RecyclerView
import com.last3oy.mobilephonebuyer.databinding.ItemImageBinding
import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.squareup.picasso.Picasso


class ImageItemViewHolder(private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(image: Image) {
        Picasso.get()
                .load(image.url)
                .into(binding.ivImage)
    }
}