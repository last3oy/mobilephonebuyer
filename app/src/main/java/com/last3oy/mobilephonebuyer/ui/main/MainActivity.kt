package com.last3oy.mobilephonebuyer.ui.main

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.last3oy.mobilephonebuyer.R
import com.last3oy.mobilephonebuyer.databinding.ActivityMainBinding
import com.last3oy.mobilephonebuyer.domain.vo.NavigateEvent
import com.last3oy.mobilephonebuyer.ui.SortDialog
import com.last3oy.mobilephonebuyer.ui.detail.DetailActivity
import com.last3oy.mobilephonebuyer.util.rx.Bus
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    lateinit var mainPageAdapter: MainPageAdapter

    @Inject
    lateinit var bus: Bus

    @Inject
    lateinit var scheduler: SchedulerProvider

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainPageAdapter = MainPageAdapter(supportFragmentManager)

        with(binding.vpMain) {
            binding.tlMain.setupWithViewPager(this)
            adapter = mainPageAdapter
        }

        disposables.add(bus.listen(NavigateEvent::class.java)
                .observeOn(scheduler.main())
                .subscribe {
                    val intent = Intent(this@MainActivity, DetailActivity::class.java)
                    intent.putExtra(DetailActivity.EXTRA_ID, it.data.id)
                    startActivity(intent)
                }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_sort -> {
                SortDialog().show(supportFragmentManager, "sort")
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }
}
