package com.last3oy.mobilephonebuyer.ui

import android.support.v7.util.DiffUtil
import com.last3oy.mobilephonebuyer.domain.vo.Mobile


class MobileDiffCallback : DiffUtil.ItemCallback<Mobile>() {
    override fun areItemsTheSame(oldItem: Mobile?, newItem: Mobile?): Boolean = oldItem?.id == newItem?.id

    override fun areContentsTheSame(oldItem: Mobile?, newItem: Mobile?): Boolean = oldItem == newItem
}