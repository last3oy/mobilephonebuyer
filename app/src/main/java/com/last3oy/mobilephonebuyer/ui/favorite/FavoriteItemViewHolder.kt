package com.last3oy.mobilephonebuyer.ui.favorite

import android.support.v7.widget.RecyclerView
import com.last3oy.mobilephonebuyer.databinding.ItemFavoriteBinding
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.squareup.picasso.Picasso


class FavoriteItemViewHolder(private val binding: ItemFavoriteBinding,
                             private val onItemClick: (Mobile) -> Unit) : RecyclerView.ViewHolder(binding.root) {

    fun bind(mobile: Mobile) {
        binding.root.setOnClickListener { onItemClick(mobile) }
        binding.tvName.text = mobile.name
        binding.tvDescription.text = mobile.description
        binding.tvPrice.text = "Price: $${mobile.price}"
        binding.tvRating.text = "Rating: ${mobile.rating}"
        Picasso.get()
                .load(mobile.thumbImageUrl)
                .into(binding.ivThumbnail)
    }

}