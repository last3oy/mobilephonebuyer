package com.last3oy.mobilephonebuyer.ui.detail

import com.last3oy.mobilephonebuyer.domain.repository.MobileRepository
import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class DetailViewModel @Inject constructor(private val repository: MobileRepository,
                                          private val scheduler: SchedulerProvider)
    : Input, Output {
    private val mobileDetailDataSubject = PublishSubject.create<Pair<Mobile, List<Image>>>()

    override fun getMobileDetail(id: Int) {
        Single
                .zip(repository.mobile(id), repository.images(id),
                        BiFunction<Mobile, List<Image>, Pair<Mobile, List<Image>>> { mobile, images -> mobile to images })
                .subscribeBy(
                        onSuccess = { mobileDetailDataSubject.onNext(it) },
                        onError = {})
    }

    override fun onShowDetailData(): Observable<Pair<Mobile, List<Image>>> = mobileDetailDataSubject
}

interface Input {
    fun getMobileDetail(id: Int)
}

interface Output {
    fun onShowDetailData(): Observable<Pair<Mobile, List<Image>>>
}