package com.last3oy.mobilephonebuyer.ui.list

import com.last3oy.mobilephonebuyer.domain.repository.MobileRepository
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class MobileListViewModel @Inject constructor(private val repository: MobileRepository,
                                              private val scheduler: SchedulerProvider) : Input, Output {

    private val mobileListSubject = BehaviorSubject.create<MutableList<Mobile>>()

    private val notifyFavoriteClickedSubject = PublishSubject.create<Unit>()
    private val errorLoadDataSubject = PublishSubject.create<Throwable>()
    private val navigateDetailScreenSubject = PublishSubject.create<Mobile>()

    override fun loadMobileList(forceUpdate: Boolean) {
        repository.list(forceUpdate)
                .subscribeBy(
                        onSuccess = { mobileListSubject.onNext(it.toMutableList()) },
                        onError = { errorLoadDataSubject.onNext(it) })
    }

    override fun onItemClick(mobile: Mobile) {
        navigateDetailScreenSubject.onNext(mobile)
    }

    override fun onFavoriteClick(mobile: Mobile, isFavorite: Boolean) {
        val mobiles = mobileListSubject.value ?: return
        val index = mobiles.indexOf(mobile)
        if (index == -1) {
            return
        }

        val newMobile = mobile.copy(isFavorite = !isFavorite)
        Single.just(newMobile)
                .observeOn(scheduler.io())
                .subscribeBy(onSuccess = {
                    repository.saveMobile(it)
                    notifyFavoriteClickedSubject.onNext(Unit)
                })

        mobiles[index] = newMobile
        mobileListSubject.onNext(mobiles)
    }

    override fun onShowMobileList(): Observable<List<Mobile>> {
        return mobileListSubject.map { it.toList() }
    }

    override fun onNotifyFavoriteClicked(): Observable<Unit> = notifyFavoriteClickedSubject

    override fun onNavigateDetailScreen(): Observable<Mobile> = navigateDetailScreenSubject

    override fun onErrorLoadData(): Observable<Throwable> = errorLoadDataSubject

}

interface Input {

    fun loadMobileList(forceUpdate: Boolean)

    fun onItemClick(mobile: Mobile)

    fun onFavoriteClick(mobile: Mobile, isFavorite: Boolean)

}

interface Output {
    fun onShowMobileList(): Observable<List<Mobile>>

    fun onNotifyFavoriteClicked(): Observable<Unit>

    fun onNavigateDetailScreen(): Observable<Mobile>

    fun onErrorLoadData(): Observable<Throwable>
}

