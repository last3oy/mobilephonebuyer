package com.last3oy.mobilephonebuyer.ui.list

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import com.last3oy.mobilephonebuyer.databinding.ItemMobileBinding
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.last3oy.mobilephonebuyer.ui.MobileDiffCallback

class MobileListAdapter(private val onItemClick: (Mobile) -> Unit,
                        private val onFavoriteClick: (Mobile, Boolean) -> Unit) : ListAdapter<Mobile, MobileItemViewHolder>(MobileDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MobileItemViewHolder {
        return MobileItemViewHolder(
                ItemMobileBinding.inflate(LayoutInflater.from(parent.context),
                parent, false),
                onItemClick,
                onFavoriteClick)
    }

    override fun onBindViewHolder(holder: MobileItemViewHolder, position: Int) {
        val mobile = getItem(position)
        holder.bind(mobile)
    }
}

