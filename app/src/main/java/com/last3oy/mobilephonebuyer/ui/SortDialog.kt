package com.last3oy.mobilephonebuyer.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.last3oy.mobilephonebuyer.R
import com.last3oy.mobilephonebuyer.databinding.DialogSortBinding
import com.last3oy.mobilephonebuyer.domain.local.sharedPreference.SortPreference
import com.last3oy.mobilephonebuyer.domain.vo.SortChangeEvent
import com.last3oy.mobilephonebuyer.util.rx.Bus
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject


class SortDialog : DaggerAppCompatDialogFragment() {

    @Inject
    lateinit var bus: Bus

    @Inject
    lateinit var sortPreference: SortPreference

    lateinit var binding: DialogSortBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_sort, container, true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val currentSortType = sortPreference.currentSortType()
        when (currentSortType) {
            SortPreference.PRICE_LOW_TO_HIGH -> binding.rgSortType.check(binding.rbPriceLow.id)
            SortPreference.PRICE_HIGH_TO_LOW -> binding.rgSortType.check(binding.rbPriceHigh.id)
            SortPreference.RATING_HIGH_TO_LOW -> binding.rgSortType.check(binding.rbRatingHigh.id)
        }

        binding.rgSortType.setOnCheckedChangeListener { radioGroup, _ ->
            when (radioGroup.checkedRadioButtonId) {
                R.id.rb_price_low -> {
                    sortPreference.saveSortType(SortPreference.PRICE_LOW_TO_HIGH)
                }
                R.id.rb_price_high -> {
                    sortPreference.saveSortType(SortPreference.PRICE_HIGH_TO_LOW)
                }
                R.id.rb_rating_high -> {
                    sortPreference.saveSortType(SortPreference.RATING_HIGH_TO_LOW)
                }
            }

            bus.publish(SortChangeEvent())
            dismiss()
        }
    }
}