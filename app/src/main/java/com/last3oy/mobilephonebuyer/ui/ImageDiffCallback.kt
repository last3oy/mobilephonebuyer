package com.last3oy.mobilephonebuyer.ui

import android.support.v7.util.DiffUtil
import com.last3oy.mobilephonebuyer.domain.vo.Image


class ImageDiffCallback : DiffUtil.ItemCallback<Image>() {
    override fun areItemsTheSame(oldItem: Image?, newItem: Image?): Boolean = oldItem?.id == newItem?.id

    override fun areContentsTheSame(oldItem: Image?, newItem: Image?): Boolean = oldItem == newItem
}