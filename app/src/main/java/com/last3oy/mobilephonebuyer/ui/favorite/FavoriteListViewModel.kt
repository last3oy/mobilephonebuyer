package com.last3oy.mobilephonebuyer.ui.favorite

import com.last3oy.mobilephonebuyer.domain.repository.MobileRepository
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class FavoriteListViewModel @Inject constructor(private val repository: MobileRepository) : Input, Output {

    private val showFavoriteListSubject = PublishSubject.create<List<Mobile>>()

    override fun loadFavoriteList() {
        repository.favoriteList()
                .subscribeBy(
                        onSuccess = { showFavoriteListSubject.onNext(it) }
                )
    }

    override fun onShowFavoriteList(): Observable<List<Mobile>> {
        return showFavoriteListSubject
    }

}

interface Input {

    fun loadFavoriteList()
}

interface Output {

    fun onShowFavoriteList(): Observable<List<Mobile>>

}