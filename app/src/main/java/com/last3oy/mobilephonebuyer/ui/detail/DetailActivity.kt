package com.last3oy.mobilephonebuyer.ui.detail

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import com.last3oy.mobilephonebuyer.R
import com.last3oy.mobilephonebuyer.databinding.ActivityDetailBinding
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject


class DetailActivity : DaggerAppCompatActivity() {

    companion object {
        const val EXTRA_ID = "id"
    }

    @Inject
    lateinit var scheduler: SchedulerProvider

    @Inject
    lateinit var viewModel: DetailViewModel

    private val adapter: ImagesAdapter by lazy { ImagesAdapter() }

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        with(binding.rvImages) {
            PagerSnapHelper().attachToRecyclerView(this)
            adapter = this@DetailActivity.adapter
            layoutManager = LinearLayoutManager(this@DetailActivity, LinearLayoutManager.HORIZONTAL, false)
        }

        val id = intent.getIntExtra(EXTRA_ID, -1)
        if (id == -1) return

        bindViewModel()

        viewModel.getMobileDetail(id)
    }

    private fun bindViewModel() {
        viewModel.onShowDetailData()
                .observeOn(scheduler.main())
                .subscribeBy(onNext = {
                    val mobile = it.first
                    val images = it.second

                    binding.tvDescription.text = mobile.description
                    binding.tvPrice.text = "Price: $${mobile.price}"
                    binding.tvRating.text = "Rating: ${mobile.rating}"

                    adapter.submitList(images)
                })
    }
}