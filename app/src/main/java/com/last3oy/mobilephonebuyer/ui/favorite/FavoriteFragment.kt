package com.last3oy.mobilephonebuyer.ui.favorite

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.last3oy.mobilephonebuyer.R
import com.last3oy.mobilephonebuyer.databinding.FragmentListBinding
import com.last3oy.mobilephonebuyer.domain.vo.FavoriteClickEvent
import com.last3oy.mobilephonebuyer.domain.vo.SortChangeEvent
import com.last3oy.mobilephonebuyer.ui.MarginItemDecoration
import com.last3oy.mobilephonebuyer.util.rx.Bus
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject


class FavoriteFragment : DaggerFragment() {

    private lateinit var binding: FragmentListBinding

    private lateinit var adapter: FavoriteListAdapter

    @Inject
    lateinit var viewModel: FavoriteListViewModel

    @Inject
    lateinit var scheduler: SchedulerProvider

    @Inject
    lateinit var bus: Bus

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false)
        adapter = FavoriteListAdapter(onItemClick = {

        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvList.apply {
            adapter = this@FavoriteFragment.adapter
            layoutManager = LinearLayoutManager(requireContext())
            val marginBottom = (resources.displayMetrics.density * 16).toInt()
            addItemDecoration(MarginItemDecoration(marginBottom))
            itemAnimator = DefaultItemAnimator()
        }

        bindObservable()

    }

    private fun bindObservable() {
        disposables.addAll(

                viewModel
                        .onShowFavoriteList()
                        .observeOn(scheduler.main())
                        .subscribeBy(onNext = {
                            adapter.submitList(it)
                        }),

                bus.listen(FavoriteClickEvent::class.java)
                        .observeOn(scheduler.main())
                        .subscribeBy(onNext = {
                            viewModel.loadFavoriteList()
                        }),

                bus.listen(SortChangeEvent::class.java)
                        .observeOn(scheduler.main())
                        .subscribe { viewModel.loadFavoriteList() }
        )
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadFavoriteList()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }
}