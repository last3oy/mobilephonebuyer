package com.last3oy.mobilephonebuyer.ui.list

import android.support.v7.widget.RecyclerView
import com.last3oy.mobilephonebuyer.databinding.ItemMobileBinding
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.squareup.picasso.Picasso

class MobileItemViewHolder(private val binding: ItemMobileBinding,
                           private val onItemClick: (Mobile) -> Unit,
                           private val onFavoriteClick: (Mobile, Boolean) -> Unit) : RecyclerView.ViewHolder(binding.root) {

    fun bind(mobile: Mobile) {
        binding.root.setOnClickListener { onItemClick(mobile) }
        binding.ivFavorite.setOnClickListener { onFavoriteClick(mobile, binding.ivFavorite.isActivated) }
        binding.tvName.text = mobile.name
        binding.tvDescription.text = mobile.description
        binding.tvPrice.text = "Price: $${mobile.price}"
        binding.tvRating.text = "Rating: ${mobile.rating}"
        binding.ivFavorite.isActivated = mobile.isFavorite
        Picasso.get()
                .load(mobile.thumbImageUrl)
                .into(binding.ivThumbnail)
    }
}