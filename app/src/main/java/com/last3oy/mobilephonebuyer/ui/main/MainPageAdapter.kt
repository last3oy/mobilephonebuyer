package com.last3oy.mobilephonebuyer.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.last3oy.mobilephonebuyer.ui.favorite.FavoriteFragment
import com.last3oy.mobilephonebuyer.ui.list.MobileListFragment

class MainPageAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {
    override fun getItem(position: Int): Fragment =
            when (position) {
                0 -> MobileListFragment()
                else -> FavoriteFragment()
            }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? =
            when (position) {
                0 -> "Mobile List"
                else -> "Favorite List"
            }
}