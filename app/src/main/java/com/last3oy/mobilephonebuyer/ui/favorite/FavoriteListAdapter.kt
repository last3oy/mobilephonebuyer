package com.last3oy.mobilephonebuyer.ui.favorite

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import com.last3oy.mobilephonebuyer.databinding.ItemFavoriteBinding
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.last3oy.mobilephonebuyer.ui.MobileDiffCallback


class FavoriteListAdapter(private val onItemClick: (Mobile) -> Unit) : ListAdapter<Mobile, FavoriteItemViewHolder>(MobileDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteItemViewHolder {
        return FavoriteItemViewHolder(ItemFavoriteBinding.inflate(LayoutInflater.from(parent.context),
                parent, false),
                onItemClick)
    }

    override fun onBindViewHolder(holder: FavoriteItemViewHolder, position: Int) {
        val favoriteMobile = getItem(position)
        holder.bind(favoriteMobile)
    }

}