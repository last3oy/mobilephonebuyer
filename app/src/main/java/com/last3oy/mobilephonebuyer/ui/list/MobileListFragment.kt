package com.last3oy.mobilephonebuyer.ui.list

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.last3oy.mobilephonebuyer.R
import com.last3oy.mobilephonebuyer.databinding.FragmentListBinding
import com.last3oy.mobilephonebuyer.domain.vo.FavoriteClickEvent
import com.last3oy.mobilephonebuyer.domain.vo.NavigateEvent
import com.last3oy.mobilephonebuyer.domain.vo.SortChangeEvent
import com.last3oy.mobilephonebuyer.ui.MarginItemDecoration
import com.last3oy.mobilephonebuyer.ui.SortDialog
import com.last3oy.mobilephonebuyer.util.rx.Bus
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject


class MobileListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModel: MobileListViewModel

    @Inject
    lateinit var scheduler: SchedulerProvider

    @Inject
    lateinit var bus: Bus

    private lateinit var binding: FragmentListBinding

    private lateinit var adapter: MobileListAdapter

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false)
        adapter = MobileListAdapter(
                onItemClick = { viewModel.onItemClick(it) },
                onFavoriteClick = { mobile, isFavorite ->
                    viewModel.onFavoriteClick(mobile, isFavorite)
                })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvList.apply {
            adapter = this@MobileListFragment.adapter
            layoutManager = LinearLayoutManager(requireContext())
            val marginBottom = (resources.displayMetrics.density * 16).toInt()
            addItemDecoration(MarginItemDecoration(marginBottom))
            itemAnimator = DefaultItemAnimator()
        }

        bindObservable()
    }

    private fun bindObservable() {
        disposables.addAll(
                viewModel.onShowMobileList()
                        .observeOn(scheduler.main())
                        .subscribeBy(onNext = { list -> adapter.submitList(list) }),

                viewModel.onNotifyFavoriteClicked()
                        .observeOn(scheduler.main())
                        .subscribeBy(onNext = { bus.publish(FavoriteClickEvent()) }),

                viewModel.onErrorLoadData()
                        .observeOn(scheduler.main())
                        .subscribeBy(
                                onNext = {
                                    Toast
                                            .makeText(requireContext(),
                                                    "Can't load mobile list.", Toast.LENGTH_LONG)
                                            .show()
                                }),

                viewModel.onNavigateDetailScreen()
                        .observeOn(scheduler.main())
                        .subscribeBy(onNext = { bus.publish(NavigateEvent(it)) }),

                bus.listen(SortChangeEvent::class.java)
                        .observeOn(scheduler.main())
                        .subscribe { viewModel.loadMobileList(false) }
        )
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadMobileList(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}