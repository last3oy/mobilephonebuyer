package com.last3oy.mobilephonebuyer.ui.detail

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import com.last3oy.mobilephonebuyer.databinding.ItemImageBinding
import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.ui.ImageDiffCallback


class ImagesAdapter : ListAdapter<Image, ImageItemViewHolder>(ImageDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageItemViewHolder {
        return ImageItemViewHolder(ItemImageBinding.inflate(LayoutInflater.from(parent.context),
                parent, false))
    }

    override fun onBindViewHolder(holder: ImageItemViewHolder, position: Int) {
        val image = getItem(position)
        holder.bind(image)
    }
}