package com.last3oy.mobilephonebuyer.di

import com.last3oy.mobilephonebuyer.ui.detail.DetailActivity
import com.last3oy.mobilephonebuyer.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindDetailActivity(): DetailActivity
}