package com.last3oy.mobilephonebuyer.di

import com.last3oy.mobilephonebuyer.ui.SortDialog
import com.last3oy.mobilephonebuyer.ui.favorite.FavoriteFragment
import com.last3oy.mobilephonebuyer.ui.list.MobileListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun bindMobileListFragmenty(): MobileListFragment

    @ContributesAndroidInjector
    abstract fun bindFavoriteListFragmenty(): FavoriteFragment

    @ContributesAndroidInjector
    abstract fun bindSortDialog(): SortDialog

}