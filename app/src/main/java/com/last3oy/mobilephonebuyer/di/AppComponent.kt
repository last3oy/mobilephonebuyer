package com.last3oy.mobilephonebuyer.di

import com.last3oy.mobilephonebuyer.MpbApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    DomainModule::class,
    RxModule::class,
    AppModule::class,
    ActivitiesModule::class])
interface AppComponent : AndroidInjector<MpbApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MpbApplication>()
}