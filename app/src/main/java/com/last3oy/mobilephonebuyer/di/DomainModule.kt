package com.last3oy.mobilephonebuyer.di

import android.arch.persistence.room.Room
import android.content.Context
import com.last3oy.mobilephonebuyer.domain.local.db.MobileDao
import com.last3oy.mobilephonebuyer.domain.local.sharedPreference.SortPreference
import com.last3oy.mobilephonebuyer.domain.remote.api.MobileService
import com.last3oy.mobilephonebuyer.domain.local.db.MobileDatabase
import com.last3oy.mobilephonebuyer.util.rx.Bus
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideFilterPreference(context: Context) = SortPreference(context)

    @Provides
    @Singleton
    fun provideMobileService(): MobileService = Retrofit
            .Builder()
            .baseUrl("https://scb-test-mobile.herokuapp.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .run { create(MobileService::class.java) }

    @Provides
    @Singleton
    fun provideMobileDatabase(context: Context): MobileDatabase = Room
            .databaseBuilder(context, MobileDatabase::class.java, "mobile-database.db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideMobileDao(db: MobileDatabase): MobileDao = db.mobileDao()


    @Provides
    @Singleton
    fun provideBus() = Bus()

}