package com.last3oy.mobilephonebuyer.di

import android.content.Context
import com.last3oy.mobilephonebuyer.MpbApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {
    @Singleton
    @Provides
    fun provideContext(application: MpbApplication): Context = application
}