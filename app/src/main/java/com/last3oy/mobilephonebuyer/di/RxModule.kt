package com.last3oy.mobilephonebuyer.di

import com.last3oy.mobilephonebuyer.util.rx.RxScheduler
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RxModule {

    @Singleton
    @Provides
    fun provideRxSchedule(): SchedulerProvider = RxScheduler()
}