package com.last3oy.mobilephonebuyer.domain.local.sharedPreference

import android.content.Context
import android.content.SharedPreferences


class SortPreference(private val context: Context) {

    companion object {
        const val PRICE_LOW_TO_HIGH = 0
        const val PRICE_HIGH_TO_LOW = 1
        const val RATING_HIGH_TO_LOW = 2

        private const val PREFERENCE_NAME = "sort_preference"
        private const val SORT_KEY = "sort_type"
    }

    private val sharedPreferences: SharedPreferences = context
            .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun saveSortType(type: Int) {
        sharedPreferences
                .edit()
                .putInt(SORT_KEY, type)
                .apply()
    }

    fun currentSortType(): Int = sharedPreferences.getInt(SORT_KEY, PRICE_LOW_TO_HIGH)

}