package com.last3oy.mobilephonebuyer.domain.repository

import android.support.annotation.WorkerThread
import com.last3oy.mobilephonebuyer.domain.MobileDataSource
import com.last3oy.mobilephonebuyer.domain.local.MobileLocalDataSource
import com.last3oy.mobilephonebuyer.domain.local.sharedPreference.SortPreference
import com.last3oy.mobilephonebuyer.domain.remote.MobileRemoteDataSource
import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MobileRepository
@Inject constructor(
        private val remote: MobileRemoteDataSource,
        private val local: MobileLocalDataSource,
        private val sortPreference: SortPreference)
    : MobileDataSource {

    override fun mobile(id: Int): Single<Mobile> {
        return local.mobile(id)
    }

    override fun list(isUpdate: Boolean): Single<List<Mobile>> {
        return if (isUpdate) {
            remote
                    .list(isUpdate)
                    .doOnSuccess { local.saveMobiles(it) }
                    .flatMap { local.list() }
                    .map { sortMobiles(it, sortPreference.currentSortType()) }
        } else {
            local
                    .list()
                    .map { sortMobiles(it, sortPreference.currentSortType()) }
        }
    }


    override fun favoriteList(): Single<List<Mobile>> {
        return local
                .favoriteList()
                .map { sortMobiles(it, sortPreference.currentSortType()) }
    }

    override fun images(id: Int): Single<List<Image>> {
        return remote.images(id)
    }

    @WorkerThread
    override fun saveMobiles(mobiles: List<Mobile>) {
        local.saveMobiles(mobiles)
    }

    @WorkerThread
    override fun saveMobile(mobile: Mobile) {
        local.saveMobile(mobile)
    }

    private fun sortMobiles(mobiles: List<Mobile>, currentSortType: Int): List<Mobile> {
        return when (currentSortType) {
            SortPreference.PRICE_LOW_TO_HIGH -> mobiles.sortedBy { it.price }

            SortPreference.PRICE_HIGH_TO_LOW -> mobiles.sortedByDescending { it.price }

            SortPreference.RATING_HIGH_TO_LOW -> mobiles.sortedByDescending { it.rating }

            else -> mobiles
        }
    }
}
