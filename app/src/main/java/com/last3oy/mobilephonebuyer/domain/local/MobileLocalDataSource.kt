package com.last3oy.mobilephonebuyer.domain.local

import com.last3oy.mobilephonebuyer.domain.MobileDataSource
import com.last3oy.mobilephonebuyer.domain.local.db.MobileDao
import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MobileLocalDataSource @Inject constructor(private val mobileDao: MobileDao,
                                                private val scheduler: SchedulerProvider)
    : MobileDataSource {

    override fun mobile(id: Int): Single<Mobile> {
        return mobileDao
                .find(id)
                .subscribeOn(scheduler.io())
    }

    override fun list(isUpdate: Boolean): Single<List<Mobile>> {
        return mobileDao
                .list()
                .subscribeOn(scheduler.io())
    }

    override fun favoriteList(): Single<List<Mobile>> = mobileDao
            .favoriteList()
            .subscribeOn(scheduler.io())

    override fun images(id: Int): Single<List<Image>> {
        return Single.never<List<Image>>()
    }

    override fun saveMobiles(mobiles: List<Mobile>) {
        mobileDao.insertMobiles(mobiles)
    }

    override fun saveMobile(mobile: Mobile) {
        mobileDao.insertOrReplace(mobile)
    }
}
