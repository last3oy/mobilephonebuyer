package com.last3oy.mobilephonebuyer.domain.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.last3oy.mobilephonebuyer.domain.vo.Mobile

@Database(entities = [Mobile::class], version = 1, exportSchema = false)
abstract class MobileDatabase : RoomDatabase() {

    abstract fun mobileDao(): MobileDao
}