package com.last3oy.mobilephonebuyer.domain.vo

import com.google.gson.annotations.SerializedName


data class Image(
        @SerializedName("id") val id: Int,
        @SerializedName("mobile_id") val mobileId: Int,
        @SerializedName("url") val url: String)