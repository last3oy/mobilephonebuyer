package com.last3oy.mobilephonebuyer.domain.remote.api

import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface MobileService {

    @GET("mobiles")
    fun mobiles(): Single<List<Mobile>>

    @GET("mobiles/{id}/images")
    fun mobileIdImages(@Path("id") mobileId: Int): Single<List<Image>>
}