package com.last3oy.mobilephonebuyer.domain.vo

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Mobile(
        @SerializedName("id") @PrimaryKey val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("brand") val brand: String,
        @SerializedName("price") val price: Float,
        @SerializedName("description") val description: String,
        @SerializedName("thumbImageURL") @ColumnInfo(name = "thumb_image_url") val thumbImageUrl: String,
        @SerializedName("rating") val rating: Float,
        val isFavorite: Boolean = false
)