package com.last3oy.mobilephonebuyer.domain

import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import io.reactivex.Single


interface MobileDataSource {

    fun list(isUpdate: Boolean = false): Single<List<Mobile>>

    fun mobile(id: Int): Single<Mobile>

    fun favoriteList(): Single<List<Mobile>>

    fun images(id: Int): Single<List<Image>>

    fun saveMobiles(mobiles: List<Mobile>)

    fun saveMobile(mobile: Mobile)
}