package com.last3oy.mobilephonebuyer.domain.local.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import io.reactivex.Single

@Dao
interface MobileDao {

    @Query("SELECT * FROM mobile")
    fun list(): Single<List<Mobile>>

    @Query("SELECT * FROM mobile WHERE isFavorite = 1")
    fun favoriteList(): Single<List<Mobile>>

    @Query("SELECT * FROM mobile WHERE id = :id")
    fun find(id: Int): Single<Mobile>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMobiles(mobiles: List<Mobile>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrReplace(vararg mobile: Mobile)
}