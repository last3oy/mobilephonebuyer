package com.last3oy.mobilephonebuyer.domain.vo


data class NavigateEvent(val data: Mobile)