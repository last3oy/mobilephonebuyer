package com.last3oy.mobilephonebuyer.domain.remote

import com.last3oy.mobilephonebuyer.domain.MobileDataSource
import com.last3oy.mobilephonebuyer.domain.remote.api.MobileService
import com.last3oy.mobilephonebuyer.domain.vo.Image
import com.last3oy.mobilephonebuyer.domain.vo.Mobile
import com.last3oy.mobilephonebuyer.util.rx.SchedulerProvider
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MobileRemoteDataSource @Inject constructor(private val mobileService: MobileService,
                                                 private val scheduler: SchedulerProvider)
    : MobileDataSource {

    override fun list(isUpdate: Boolean): Single<List<Mobile>> {
        return if (isUpdate) {
            mobileService
                    .mobiles()
                    .subscribeOn(scheduler.io())
        } else {
            Single.never<List<Mobile>>()
        }
    }

    override fun favoriteList(): Single<List<Mobile>> = Single.never()

    override fun images(id: Int): Single<List<Image>> = mobileService
            .mobileIdImages(id)
            .subscribeOn(scheduler.io())

    override fun saveMobiles(mobiles: List<Mobile>) {

    }

    override fun mobile(id: Int): Single<Mobile> {
        return Single.never<Mobile>()
    }

    override fun saveMobile(mobile: Mobile) {

    }
}