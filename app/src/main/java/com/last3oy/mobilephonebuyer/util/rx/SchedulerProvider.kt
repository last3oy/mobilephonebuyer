package com.last3oy.mobilephonebuyer.util.rx

import io.reactivex.Scheduler


interface SchedulerProvider {

    fun io(): Scheduler

    fun main(): Scheduler

    fun computation(): Scheduler
}