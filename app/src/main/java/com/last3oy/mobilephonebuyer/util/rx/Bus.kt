package com.last3oy.mobilephonebuyer.util.rx

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


class Bus {

    private val publish = PublishSubject.create<Any>()

    fun publish(any: Any) {
        publish.onNext(any)
    }

    fun <T> listen(clazz: Class<T>) : Observable<T> = publish.ofType(clazz)
}